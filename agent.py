"""
@author: simonpresern
This is short version of Agent object.
-Description of its behaviour in "external" enviroment (more scripts needed to make it functional)
-RL model was developed to trade on high frequency market
"""

import keras
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense
from keras.optimizers import Adam

import numpy as np
import pandas as pd
import random
from collections import deque

def getPositionVariables(state):
    myDict=dict()
    myDict['current ask']=state[0]
    myDict['current bid']=state[1]
    myDict['current Price']=state[3]
    myDict['is neutral']=state[-1]
    myDict['how short']=state[-2]
    myDict['is short']=state[-3]
    myDict['how long']=state[-4]
    myDict['is long']=state[-5]
    myDict['current profit']=state[-6]
    return myDict

def avg(myList):
    return sum(myList)/len(myList)
    
class Agent:
    def __init__(self, state_size, is_eval=False, model_name="",fee=0.09,win_ratio=0.6,loss_ratio=0.3):
        self.state_size = state_size # normalized previous days
        self.action_size = 3 # sit, buy, sell
        self.memory = deque(maxlen=10000)
        self.Inventory_Sell = []
        self.Inventory_Buy = []
        self.model_name = model_name
        self.is_eval = is_eval
        self.win_ratio = win_ratio 
        self.loss_ratio = loss_ratio
        
        self.epsilon = 1
        self.ValueTable=[]
        self.model = load_model("models/" + model_name) if model_name!="" else self._model()
        self.fee=fee
       
        self.X=None
        self.y=None
    #model = load_model("models/model_ep0") if is_eval else self._model()

   
    def _model(self):
        model = Sequential()
        model.add(Dense(units=64, input_dim=self.state_size, activation="relu"))
        model.add(Dense(units=32, activation="relu"))
        model.add(Dense(units=8, activation="relu"))
        #output vektor = action_size
        model.add(Dense(self.action_size, activation="linear"))
        model.compile(loss="mse", optimizer=Adam(lr=0.001))

        return model

    def act(self, state):
        #explore!
        if not self.is_eval and random.random() <= self.epsilon:
            return random.randrange(self.action_size)
        #choose action
        options = self.model.predict_on_batch(np.expand_dims(state[2:],axis=0))[0]
        print('options:' + str(options))
        return np.argmax(options)

    def checkPosition(self,state):
        evaluate_pos = (state[-5]/state[-5] if state[-5]!=0 else 0) + (state[-3]/state[-3] if state[-3]!=0 else 0) +state[-1]
        if(evaluate_pos!=1):
            print(state[-5],state[-3],state[-1])
            raise Exception('Odprt je lahko natanko en tip pozicije')

    
    def expReplay(self,e,model_name,advanced=False):
     
        X=[]
        y=[]
        implied_reward=0
        idd=0
        

        #go through batch and reward/punish the agent
        self.ValueTable=[]
        allStates=[]
        for state, action, reward, done in reversed(self.memory):
             allStates+=[state[2:]]
        npAll=np.asarray(allStates)
        targetAll=self.model.predict(npAll)
        
        i=-1
        prices = list()
        train_s=list()
        next_state_index = -1
        for state, action, reward, done in reversed(self.memory):
            i+=1
               
            position=getPositionVariables(state)            
            self.checkPosition(state)
            
            target_f=targetAll[i]
            
            if done:
                target_f[action] = reward
            else:
                #we are opening position
                if(position['is neutral']==1):
                    target_f[0] = 0
                    if (max(prices) - position['current Price'] - self.fee) > 0:
                        target_f[1] = self.win_ratio*(max(prices) - position['current Price'] - self.fee)
                    else:
                        target_f[1] = (max(prices) - position['current Price'] - self.fee)
                    if (position['current Price'] - min(prices) - self.fee) > 0:
                        target_f[2] =  self.win_ratio*(position['current Price'] - min(prices) - self.fee)
                    else:
                        target_f[2] =  (position['current Price'] - min(prices) - self.fee)
            
                #position in already opened
                if(position['is neutral']==0):
                    if position['is short']>0:
                        future_profit = 0.5*self.win_ratio*(max(prices) - position['current Price'] - self.fee)
                        target_f[1] = position['is short']*(position['how short'] - position['current Price']) + (future_profit if future_profit>0 else 0)
                        if (position['how short'] - min(prices) - self.fee) > 0:
                            target_f[0] = self.win_ratio*position['is short']*(position['how short'] - min(prices) - self.fee)
                            target_f[2] = self.win_ratio*position['is short']*((position['how short']*position['is short']+ position['current Price'])/(position['is short']+1) - min(prices) - self.fee)
                        else:
                            target_f[0] = self.loss_ratio*position['is short']*(position['how short'] - min(prices) - self.fee)
                            target_f[2] = self.loss_ratio*position['is short']*((position['how short']*position['is short']+ position['current Price'])/(position['is short']+1) - min(prices) - self.fee)
                        
          
                    if position['is long']>0:
                        future_profit = (0.5*self.win_ratio*(position['current Price']-min(prices) - self.fee))
                        target_f[2] = position['is long']*(position['current Price'] - position['how long'] - self.fee) + (future_profit if future_profit>0 else 0)
                        if (max(prices) - position['how long'] - self.fee)>0: 
                            target_f[0] = self.win_ratio*position['is long']*(max(prices) - position['how long'] - self.fee)
                            target_f[1] = self.win_ratio*position['is long']*(max(prices) - (position['how long']*position['is long']+ position['current Price'])/(position['is long']+1) - self.fee)
                        else:
                            target_f[0] = self.loss_ratio*position['is long']*(max(prices) - position['how long'] - self.fee)
                            target_f[1] = self.loss_ratio*position['is long']*(max(prices) - (position['how long']*position['is long']+ position['current Price'])/(position['is long']+1) - self.fee)
                    idd+=1
    
                reward=implied_reward
                
            prices.append(position['current Price'])
            self.ValueTable+=[[state[0],state[1],target_f[0],target_f[1],target_f[2],action,reward,state[-5],state[-4],state[-3],state[-2],state[-1],idd]]
            X+=[state[2:]]
            y+=[target_f]
            
        X=np.asarray(X)
        y=np.asarray(y)

    
        self.X=X
        self.y=y
        self.model.fit(X, y, epochs=1, verbose=0)
        self.memory.clear()
        return self.ValueTable
        
        