import os
from functions import *
import sys
from agent.agent import Agent
import numpy as np
import pandas as pd
import datetime as dt
import getDEintradayData as da
import matplotlib.pyplot as plt
import random
import copy
import functions as f
import time
import evaluate

fee=0.09
agent.fee=0.09
delezSpread=0
start=time.time()
positions=[0,0,0]
for e in range(500000):
    
    if(e%1000==0 and e!=0):
        end=time.time()
        print('ep: '+str(e)+"; time el.: "+str(end-start))
        print("avg profit: "+str(sum(profits)/len(profits))+" ; postions:"+str(positions)+" ; epsilon: "+str(agent.epsilon))
        profits=[]
        positions=[0,0,0]
        start=time.time()
                                                                                                                                                                                                                                                                                                                                                                                                                                
    choosen_product = random.choice(products)
    ProductData= productDict[choosen_product]

    data=copy.deepcopy(ProductData)
    #izničimo spread za lažjo konvergenco
    if(random.random()>delezSpread):
        data[:,0]=(data[:,1]+data[:,0])/2
        data[:,1]=data[:,0]
    l = len(data)
    
    total_profit = 0
    agent.Inventory_Sell = []
    agent.Inventory_Buy = []

        
    t = 0
    while t < l:
        state = f.getState(data, t)

        reward=0
        locationOfOpenedPosition=0
        currentAsk=data[t][0]
        currentBid=data[t][1]
        done = True if t == l - 1 else False
        #opis odprte pozicije--oznake so ravno obratne, ker buy pozicija omogoča sell operacijo
        if(len(agent.Inventory_Sell)>0):
           state[-2]=agent.Inventory_Sell[0] 
           state[-3]=1
           sold_price = agent.Inventory_Sell[0]
           state[-6] = sold_price - currentAsk - fee
        elif(len(agent.Inventory_Buy)>0):
           state[-4]=agent.Inventory_Buy[0]
           state[-5]=1
           bought_price = agent.Inventory_Buy[0]
           state[-6] = currentBid - bought_price - fee
        else:
           state[-1]=1

        action = agent.act(state,printt=False)
        #print(action, state[-3],state[-5])
        
        if(action == 1 and (len(agent.Inventory_Sell)==1 or len(agent.Inventory_Buy)==0)):
            if(len(agent.Inventory_Sell)==1):
                sold_price = agent.Inventory_Sell.pop(0)
                reward = sold_price - currentAsk - fee
                total_profit += reward
                if log_console:
                    print("Buy: " + f.formatPrice(currentBid) + " | Profit: " + f.formatPrice(reward))
                #t-=1
            else:
                locationOfOpenedPosition=t
                agent.Inventory_Buy.append(currentBid)
                if log_console:
                    print("Buy: " + f.formatPrice(currentBid))

        elif( action == 2 and (len(agent.Inventory_Buy)==1 or len(agent.Inventory_Sell)==0)): # sell
            if(len(agent.Inventory_Buy)==1):
                bought_price = agent.Inventory_Buy.pop(0)
                reward = currentBid - bought_price - fee
                total_profit += reward
                if log_console:
                    print("Sell: " + f.formatPrice(currentBid) + " | Profit: " + f.formatPrice(reward))
                #t-=1
            else:
                agent.Inventory_Sell.append(currentBid)
                #print("Sell: " + formatPrice(currentBid))     
        elif (done and len(agent.Inventory_Buy) == 1):
            bought_price = agent.Inventory_Buy.pop(0)
            reward = currentBid - bought_price - fee
            total_profit += reward
            action = 2
            if log_console:
                print("Ending with open position long. Forced closed position with reward {}".format(reward))
        elif (done and len(agent.Inventory_Sell) == 1):
            sold_price = agent.Inventory_Sell.pop(0)
            reward = sold_price - currentAsk - fee
            total_profit += reward
            action = 1
            if log_console:
                print("Ending with open position short. Forced closed position with reward {}".format(reward))
        else:
            action=0
            #print("Stay: ")

        agent.memory.append((state, action, reward, done))
        if (done):
            #print("--------------------------------")
            #print("Total Profit: " + formatPrice(total_profit))
            profits+=[total_profit]
            list_of_total_profits.append(total_profit)
            list_of_epsilons.append(agent.epsilon)
            #print("--------------------------------")
        t+=1
        positions[action]+=1
    agent.expReplay()                                                                           
        
    if (e % 9000 == 0):
        #zmanjšamo epsilon
        if (agent.epsilon >0.04):
            agent.epsilon=agent.epsilon*0.925
        if (e>200000):
            if(fee<finalFee):
                fee+=0.000
            if(delezSpread<1.0):
                delezSpread+=0.00
        print("episode: "+str(e))
        #print("dogodkov c1: "+str(agent.c1)+", dokodkov c2: "+str(agent.c2))


t=agent.ValueTable
tp=pd.DataFrame(t)
tp=tp.sort_index(ascending=False, axis=0)
tp.columns=['trenutnAsk','trenutniBid','stay pred','buy pred','sell pred','org stay','org buy','org sell','akcija','nagrada','is_Long','how_Long','is_Short','how_Short','is_Neutral','idd']


